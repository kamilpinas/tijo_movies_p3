package pl.edu.pwsztar.domain.dto;

public class CounterDto {
    private long counter;

    public CounterDto(long counter) {
        this.counter = counter;
    }
    public CounterDto() {}

    public long getCounter() {

        return counter;
    }



    @Override
    public String toString() {
        return "CounterDto{" +
                "counter=" + counter +
                '}';
    }
}
