package pl.edu.pwsztar.domain.dto;

public class UpdateMovieDto {
    private String title;
    private String videoId;
    private String image;
    private Integer year;

    public UpdateMovieDto() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public String getVideoId() {
        return videoId;
    }

    @Override
    public String toString() {
        return "UpdateMovieDto{" +
                "title='" + title + '\'' +
                ", videoId='" + videoId + '\'' +
                ", image='" + image + '\'' +
                ", year=" + year +
                '}';
    }
}
