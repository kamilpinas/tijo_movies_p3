package pl.edu.pwsztar.service;


import pl.edu.pwsztar.domain.dto.*;

import java.util.List;

public interface MovieService {

    List<MovieDto> findAll();

    void creatMovie(CreateMovieDto createMovieDto);

    void deleteMovie(Long movieId);

    CounterDto countMov();

    void updateMovie(UpdateMovieDto updateMovieDto,long x);

    DetailsMovieDto findMovie(Long movieId);
}
